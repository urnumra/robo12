#include "PS2X_lib.h"

PS2X ps2x; // создание устройства типа "контроллер"

const int del = 100;

const int LeftMotorPin = 3;
const int RightMotorPin = 9;
const int RightMotorIn1 = 4;
const int RightMotorIn2 = 5;
const int LeftMotorIn1 = 6;
const int LeftMotorIn2 = 7;

const int M1I1 = 19;
const int M1I2 = 18;
const int M2I1 = 17;
const int M2I2 = 16;

const int MAX_SPEED = 85;      // Максимальная скорость для ШИМ регулировки
const int MIN_SPEED = 30;      // Минимальная скорость для ШИМ регулировки

int Last_speed = 0;

int SPEED_TURN = 0;
int motor_speed_left = 200;
int motor_speed_right = 210;
int RightX = 0;
int RightY = 0;
int LeftMotorOutput; //will hold the calculated output for the left motor
int RightMotorOutput; //will hold the calculated output for the right motor

void steer();
void forward();
void backward();
void left();
void right();
void stop();
void TU();
void TD();
void TL();
void TR();
void TS();

void setup()
{
	ps2x.config_gamepad(10, 12, 11, 13, false, true); //настройка джойстика (clock, command, attention, data, Pressures?, Rumble?)
	pinMode(RightMotorIn1, OUTPUT);
	pinMode(RightMotorIn2, OUTPUT);
	pinMode(LeftMotorIn1, OUTPUT);
	pinMode(LeftMotorIn2, OUTPUT);
	analogWrite(LeftMotorPin, motor_speed_left);
	analogWrite(RightMotorPin, motor_speed_right);
	pinMode(M1I1,OUTPUT);
	pinMode(M1I2,OUTPUT);
	pinMode(M2I1,OUTPUT);
	pinMode(M2I2,OUTPUT);
}

void loop()
{
	ps2x.read_gamepad();
	steer();
	if (ps2x.Button(PSB_PAD_LEFT))
	{
		TR();
	}
	else if (ps2x.Button(PSB_PAD_RIGHT))
	{
		TL();
	}
	else
	{
		TS();
	}
	
	if (ps2x.Button(PSB_START))
	{

	}
	else if (ps2x.Button(PSB_SELECT))
	{

	}
	
	if (ps2x.Button(PSB_R3))
	{

	}
	else if (ps2x.Button(PSB_R3))
	{

	}
	else if (ps2x.Button(PSB_GREEN))
	{
		forward();
	}
	else if (ps2x.Button(PSB_BLUE))
	{
		backward();
	}
	else if (ps2x.Button(PSB_PINK))
	{
		left();
	}
	else if (ps2x.Button(PSB_RED))
	{
		right();
	}
	else if (ps2x.Button(PSB_R1))
	{
		
	}
	else if (ps2x.Button(PSB_R2))
	{

	}	
	else if (ps2x.Button(PSB_L1))
	{
		
	}
	else if (ps2x.Button(PSB_L2))
	{

	}
	else if (ps2x.Button(PSB_PAD_UP))
	{

	}
	else if (ps2x.Button(PSB_PAD_DOWN))
	{

	}
	else if (ps2x.Analog(PSS_LX) > 146)
	{

	}
	else if (ps2x.Analog(PSS_LX) < 110)
	{

	}
	else if (ps2x.Button(PSB_TRIANGLE))
	{

	}
	else if (ps2x.Button(PSB_CIRCLE))
	{

	}
	else if (ps2x.Button(PSB_CROSS))
	{

	}
	else if (ps2x.Button(PSB_SQUARE))
	{

	}
	else
	{
	
	}

}
void steer()
{ 
  RightX = ps2x.Analog(PSS_RX);
  RightY = ps2x.Analog(PSS_RY);
  int Speed = constrain(RightY - 127, -127, 127);
  int Speed_left  = -constrain(((Speed + (RightX - 128))), -127, 127);
  int Speed_right = -constrain(((Speed - (RightX - 128))), -127, 127);
  int Speed_absL = abs(Speed_left);
  int Speed_absR = abs(Speed_right);
  analogWrite(LeftMotorPin, Speed_absL);
  analogWrite(RightMotorPin, Speed_absR);
  digitalWrite(RightMotorIn1, constrain(map(Speed_right,  -1, 1, 0, 1), 0, 1));
  digitalWrite(RightMotorIn2, constrain(map(Speed_right,  -1, 1, 1, 0), 0, 1));
  digitalWrite(LeftMotorIn1, constrain(map(Speed_left,  -1, 1, 1, 0), 0, 1));
  digitalWrite(LeftMotorIn2, constrain(map(Speed_left,  -1, 1, 0, 1), 0, 1));
  Last_speed = Speed;
  delay(100);
}

void forward()
{
	digitalWrite(RightMotorIn1, HIGH);
	digitalWrite(RightMotorIn2, LOW);
	digitalWrite(LeftMotorIn1, HIGH);
	digitalWrite(LeftMotorIn2, LOW);
}
void backward()
{
	digitalWrite(RightMotorIn1, LOW);
	digitalWrite(RightMotorIn2, HIGH);
	digitalWrite(LeftMotorIn1, LOW);
	digitalWrite(LeftMotorIn2, HIGH);
}
void right()
{
	digitalWrite(RightMotorIn1, LOW);
	digitalWrite(RightMotorIn2, HIGH);
	digitalWrite(LeftMotorIn1, HIGH);
	digitalWrite(LeftMotorIn2, LOW);
}
void left()
{
	digitalWrite(RightMotorIn1, HIGH);
	digitalWrite(RightMotorIn2, LOW);
	digitalWrite(LeftMotorIn1, LOW);
	digitalWrite(LeftMotorIn2, HIGH);
}
void stop()
{
	digitalWrite(RightMotorIn1, LOW);
	digitalWrite(RightMotorIn2, LOW);
	digitalWrite(LeftMotorIn1, LOW);
	digitalWrite(LeftMotorIn2, LOW);
}
void TU()
{

}
void TD()
{
	
}
void TL()
{
	digitalWrite(M1I1, HIGH);
	digitalWrite(M1I2, LOW);
}
void TR()
{
	digitalWrite(M1I1, LOW);
	digitalWrite(M1I2, HIGH);
}
void TS()
{
	digitalWrite(M1I1, LOW);
	digitalWrite(M1I2, LOW);
}